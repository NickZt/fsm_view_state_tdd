# MVI: Rendering Logic of State in Android: TDD story

**To read the story**: https://codingstories.io/story/61811e66d46036002a1a08a1/https:%2F%2Fgitlab.com%2FNickZt%2Ffsm_view_state_tdd

**To code yourself**: https://gitlab.com/NickZt/fsm_view_state_tdd

**Estimated reading time**: 45 minutes

## Story Outline
Read a short story about how to test the logic of a user interface written in Kotlin in the MVI style (using FSM)

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #TDD, #kotlin, #MVI