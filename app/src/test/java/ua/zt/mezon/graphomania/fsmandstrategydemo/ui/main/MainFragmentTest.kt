package ua.zt.mezon.graphomania.fsmandstrategydemo.ui.main

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainFragmentTest : TestCase() {
    @Spy
    private lateinit var contract: MockForTestMainFragmentViewStatesRenderContract

    @Test
    fun testRenderInitState() {
        // Setup

        // Act
        contract.render(MainFragmentUiStatesModel.Initial)

        // Assert
        verify(contract).showInitState()
        verify(contract, never()).showLoadProgress(any())
        verify(contract, never()).showError(any())
        verify(contract, never()).showEmptyState()
        verify(contract, never()).showList(any())
    }

    @Test
    fun testRenderLoadCounterPercentData() {
        // Act
        contract.render(MainFragmentUiStatesModel.LoadCounterPercentDataState(50))
        // Assert
        verify(contract, never()).showInitState()
        verify(contract).showLoadProgress(50)
        verify(contract, never()).showError(any())
        verify(contract, never()).showEmptyState()
        verify(contract, never()).showList(any())
    }

    @Test
    fun testRenderLoadError() {
        // Act
        contract.render(MainFragmentUiStatesModel.LoadErrorState("Error"))
        // Assert
        verify(contract, never()).showInitState()
        verify(contract, never()).showLoadProgress(any())
        verify(contract).showError("Error")
        verify(contract, never()).showEmptyState()
        verify(contract, never()).showList(any())
    }

    @Test
    fun testRenderListEmpty() {
        // Act
        contract.render(MainFragmentUiStatesModel.ListEmptyState)
        // Assert
        verify(contract, never()).showInitState()
        verify(contract, never()).showLoadProgress(any())
        verify(contract, never()).showError(any())
        verify(contract).showEmptyState()
        verify(contract, never()).showList(any())
    }

    @Test
    fun testRenderListShow() {
        // Act
        contract.render(MainFragmentUiStatesModel.ListShowState(ArrayList()))
        // Assert
        verify(contract, never()).showInitState()
        verify(contract, never()).showLoadProgress(any())
        verify(contract, never()).showError(any())
        verify(contract, never()).showEmptyState()
        verify(contract).showList(any())
    }
}