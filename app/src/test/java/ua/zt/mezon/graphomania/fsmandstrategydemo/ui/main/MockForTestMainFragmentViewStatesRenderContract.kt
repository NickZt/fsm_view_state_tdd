package ua.zt.mezon.graphomania.fsmandstrategydemo.ui.main

import ua.zt.mezon.graphomania.fsmandstrategydemo.datasources.ItemData

open class MockForTestMainFragmentViewStatesRenderContract : MainFragmentViewStatesRenderContract {
    override fun showInitState() {
    }

    override fun showLoadProgress(percent: Int) {
    }

    override fun showError(error: String?) {
    }

    override fun showEmptyState() {
    }

    override fun showList(listItems: ArrayList<ItemData>) {
    }
}